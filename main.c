#include <stdio.h>
#include "test.h"

int main(int argc, char *argv[])
{
    base64_test_file();

    base64_test_string();

    base64_test_buffer();

    return 0;
}
