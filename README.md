# base64
c语言版base64编解码算法实现

# Base64 编码说明
Base64 是一种基于 64 个可打印字符来表示二进制数据的表示方法，由于 2^6=64，所以每 6 个比特为一个单元，对应某个可打印字符。

Base64 常用于在通常处理文本数据的场合，表示、传输、存储一些二进制数据，包括 MIME 的电子邮件及 XML 的一些复杂数据。

Base64 编码要求把 3 个 8 位字节（3\*8=24）转化为 4 个 6 位的字节（4\*6=24），之后在 6 位的前面补两个 0，形成 8 位一个字节的形式。 如果剩下的字符不足 3 个字节，则用 0 填充，输出字符使用 =，因此编码后输出的文本末尾可能会出现 1 或 2 个 =。

为了保证所输出的编码位可读字符，Base64 制定了一个编码表，以便进行统一转换。编码表的大小为 2^6=64，这也是 Base64 名称的由来。

在 Base64 中的可打印字符包括字母 A-Z、a-z、数字 0-9，这样共有 62 个字符，此外两个可打印符号在不同的系统中而不同。

以下是 Base64 编码的基本步骤：

* 将数据划分为 3 个字节一组（24位）。
* 将每个字节转换为 8 位二进制形式。
* 将 24 位数据按照 6 位一组进行划分，得到 4 个 6 位的组。
* 将每个 6 位的组转换为对应的 Base64 字符。
* 如果数据不足 3 字节，进行填充。
* 将所有转换后的 Base64 字符连接起来，形成最终的编码结果。
* 解码 Base64 编码的过程与编码相反，将每个 Base64 字符转换为对应的6位二进制值，然后将这些 6 位值组合成原始的二进制数据。

Base64 编码具有以下特点：

* 编码后的数据长度总是比原始数据长约 1/3。
* 编码后的数据可以包含 A-Z、a-z、0-9 和两个额外字符的任意组合。
* Base64 编码是一种可逆的编码方式，可以通过解码还原原始数据。

# Base64编码表
码值 |   字符     |        码值  |  字符    |       码值  |  字符    |      码值   |  字符
-----|------------|--------------|----------|-------------|----------|-------------|------
0    |    A       |        16    |    Q     |       32    |   g      |      48     |    w
1    |    B       |        17    |    R     |       33    |   h      |      49     |    x
2    |    C       |        18    |    S     |       34    |   i      |      50     |    y
3    |    D       |        19    |    T     |       35    |   j      |      51     |    z
4    |    E       |        20    |    U     |       36    |   k      |      52     |    0
5    |    F       |        21    |    V     |       37    |   l      |      53     |    1
6    |    G       |        22    |    W     |       38    |   m      |      54     |    2
7    |    H       |        23    |    X     |       39    |   n      |      55     |    3
8    |    I       |        24    |    Y     |       40    |   o      |      56     |    4
9    |    J       |        25    |    Z     |       41    |   p      |      57     |    5
10   |    K       |        26    |    a     |       42    |   q      |      58     |    6
11   |    L       |        27    |    b     |       43    |   r      |      59     |    7
12   |    M       |        28    |    c     |       44    |   s      |      60     |    8
13   |    N       |        29    |    d     |       45    |   t      |      61     |    9
14   |    O       |        30    |    e     |       46    |   u      |      62     |    +
15   |    P       |        31    |    f     |       47    |   v      |      63     |    /

# 举例
来举例说，一段引用自托马斯·霍布斯《利维坦》的文本：

    Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.

使用 Base64 编码之后变成：

    TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=

# 转换过程示例
编码 “Man” 的结果为 TWFu，转换过程如下：

文本             |  M        | a         |  n        |   |
-----------------|-----------|-----------|-----------|----
ASCII 编码       | 77        | 97        | 110       |
二进制位         | 01001101  | 01100001  | 01101110  |
二进制位(重组)   | 010011 | 010110 | 000101 | 101110 |
索引             | 19     | 22     | 5      |  46    |
Base64 编码      | T      | W      | F      |  u     |


# 示例程序运行结果展示

```sh
$ ./base64


file base64 encode/decode test:
  encode(./input.jpg) success
  decode(encode(./input.jpg)) => ./output.jpg
check md5:
4b2c00b91c9a38d4f85036f6149fc1ff  input.jpg
4b2c00b91c9a38d4f85036f6149fc1ff  output.jpg


string base64 encode/decode test:
  encode("a") => YQ==
  decode("YQ==") => a
  encode("ab") => YWI=
  decode("YWI=") => ab
  encode("abc") => YWJj
  decode("YWJj") => abc
  encode("abcd") => YWJjZA==
  decode("YWJjZA==") => abcd
  encode("abcde") => YWJjZGU=
  decode("YWJjZGU=") => abcde
  encode("abcdef") => YWJjZGVm
  decode("YWJjZGVm") => abcdef
  encode("abcdefg") => YWJjZGVmZw==
  decode("YWJjZGVmZw==") => abcdefg
  encode("abcdefgh") => YWJjZGVmZ2g=
  decode("YWJjZGVmZ2g=") => abcdefgh
  encode("abcdefghi") => YWJjZGVmZ2hp
  decode("YWJjZGVmZ2hp") => abcdefghi


Binary buffer base64 encode/decode test:
  encode(0xff 0xd8 0xff 0xe0 0 0x10 0x4a 0x46 0x49 0x46 0 0x1 0x1 0x1 0 0x7d ) => /9j/4AAQSkZJRgABAQEAfQ==
  decode("/9j/4AAQSkZJRgABAQEAfQ==") => 0xff 0xd8 0xff 0xe0 0 0x10 0x4a 0x46 0x49 0x46 0 0x1 0x1 0x1 0 0x7d
```

# 内存泄漏检测

```sh
    ==60836== Memcheck, a memory error detector
    ==60836== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
    ==60836== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
    ==60836== Command: ./base64
    ==60836==
    ==60836==
    ==60836== HEAP SUMMARY:
    ==60836==     in use at exit: 0 bytes in 0 blocks
    ==60836==   total heap usage: 14,628 allocs, 14,628 frees, 213,915 bytes allocated
    ==60836==
    ==60836== All heap blocks were freed -- no leaks are possible
    ==60836==
    ==60836== For counts of detected and suppressed errors, rerun with: -v
    ==60836== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

# 参考资料
https://c.runoob.com/front-end/693/
