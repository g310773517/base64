#ifndef __TEST_H_
#define __TEST_H_

#ifdef __cplusplus
extern "C"{
#endif

extern void base64_test_file(void);
extern void base64_test_buffer(void);
extern void base64_test_string(void);

#ifdef __cplusplus
}
#endif
#endif //__TEST_H_
